package cz.spiffyk.hexskvorky;

import lombok.val;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static cz.spiffyk.hexskvorky.TestUtil.point;

@SuppressWarnings("OptionalGetWithoutIsPresent")
class HexskvorkyTest {

    @Test
    void checkWinningCondition1() {
        val game = new Hexskvorky(3, 4);

        assertEquals(Hexskvorky.State.IN_GAME, game.getState());

        game.setValue(point(0, 0), 2);
        game.setValue(point(0, 1), 2);
        game.setValue(point(0, 2), 2);
        game.setValue(point(0, 3), 2);

        game.checkWinningCondition(point(0, 2));

        assertEquals(Hexskvorky.State.FINISHED, game.getState());
        assertEquals(2, game.getWinner());
        assertTrue(game.getWinningPoints().containsAll(List.of(
                point(0, 0),
                point(0, 1),
                point(0, 2),
                point(0, 3))));
    }

    @Test
    void checkWinningCondition2() {
        val game = new Hexskvorky(3, 4);

        assertEquals(Hexskvorky.State.IN_GAME, game.getState());

        game.setValue(point(0, 0), 1);
        game.setValue(point(-1, 1), 1);
        game.setValue(point(-2, 2), 1);
        game.setValue(point(-3, 3), 1);

        game.checkWinningCondition(point(-3, 3));

        assertEquals(Hexskvorky.State.FINISHED, game.getState());
        assertEquals(1, game.getWinner());
        assertTrue(game.getWinningPoints().containsAll(List.of(
                point(0, 0),
                point(-1, 1),
                point(-2, 2),
                point(-3, 3))));
    }

    @Test
    void checkWinningCondition3() {
        val game = new Hexskvorky(3, 4);

        assertEquals(Hexskvorky.State.IN_GAME, game.getState());

        game.setValue(point(0, 0), 1);
        game.setValue(point(-1, 1), 1);
        game.setValue(point(0, 2), 1);
        game.setValue(point(-3, 3), 1);

        game.checkWinningCondition(point(-3, 3));

        assertEquals(Hexskvorky.State.IN_GAME, game.getState());
    }

    @Test
    void checkWinningCondition_negative1() {
        val game = new Hexskvorky(3, 4);

        assertThrows(IllegalArgumentException.class, () -> game.checkWinningCondition(point(-3, 3)));
    }

    @Test
    void play1() {
        val game = new Hexskvorky(3, 4);

        assertEquals(0, game.getCurrentPlayer());

        game.play(point(0, 1));
        assertEquals(0, game.valueAt(point(0, 1)).get());
        assertEquals(1, game.getCurrentPlayer());

        game.play(point(3, 1));
        assertEquals(1, game.valueAt(point(3, 1)).get());
        assertEquals(2, game.getCurrentPlayer());

        game.play(point(5, 1));
        assertEquals(2, game.valueAt(point(5, 1)).get());
        assertEquals(0, game.getCurrentPlayer());

        game.play(point(0, 10));
        assertEquals(0, game.valueAt(point(0, 10)).get());
        assertEquals(1, game.getCurrentPlayer());
    }

    @Test
    void play_negative1() {
        val game = new Hexskvorky(3, 4);

        assertEquals(0, game.getCurrentPlayer());

        game.play(point(0, 1));
        assertEquals(0, game.valueAt(point(0, 1)).get());
        assertEquals(1, game.getCurrentPlayer());

        game.play(point(0, 1)); // game should not proceed because the point is already filled
        assertEquals(0, game.valueAt(point(0, 1)).get());
        assertEquals(1, game.getCurrentPlayer());
    }

    @Test
    void play_negative2() {
        val game = new Hexskvorky(3, 4);

        game.setValue(point(0, 0), 2);
        game.setValue(point(0, 1), 2);
        game.setValue(point(0, 2), 2);
        game.setValue(point(0, 3), 2);

        game.checkWinningCondition(point(0, 2));

        assertThrows(IllegalStateException.class, () -> game.play(point(10, 10)));
    }

}