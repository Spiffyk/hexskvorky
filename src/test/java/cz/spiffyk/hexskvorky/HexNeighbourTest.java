package cz.spiffyk.hexskvorky;

import lombok.val;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static cz.spiffyk.hexskvorky.TestUtil.point;

class HexNeighbourTest {

    @Test
    void of1() {
        val a = point(0, 1);
        val b = HexNeighbour.BOTTOM_LEFT.of(a);

        assertNotSame(a, b);
        assertNotEquals(a, b);
        assertEquals(a, point(0, 1));
        assertEquals(b, point(-1, 2));
    }

    @Test
    void of2() {
        val a = point(0, 1);
        val b = HexNeighbour.BOTTOM_LEFT.of(a, 3);

        assertNotSame(a, b);
        assertNotEquals(a, b);
        assertEquals(a, point(0, 1));
        assertEquals(b, point(-3, 4));
    }

    @Test
    void set1() {
        val a = point(0, 1);
        val b = HexNeighbour.BOTTOM_LEFT.set(a);

        assertSame(a, b);
        assertEquals(a, point(-1, 2));
    }

    @Test
    void set2() {
        val a = point(0, 1);
        val b = HexNeighbour.BOTTOM_LEFT.set(a, 5);

        assertSame(a, b);
        assertEquals(a, point(-5, 6));
    }

}