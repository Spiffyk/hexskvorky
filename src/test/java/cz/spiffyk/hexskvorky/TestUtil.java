package cz.spiffyk.hexskvorky;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.vecmath.Point2i;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TestUtil {

    /**
     * Utility method for creating points.
     */
    public static Point2i point(int x, int y) {
        return new Point2i(x, y);
    }

}
