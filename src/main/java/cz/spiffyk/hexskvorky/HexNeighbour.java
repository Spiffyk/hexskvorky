package cz.spiffyk.hexskvorky;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.vecmath.Point2i;
import java.util.List;

/**
 * A utility enumeration for calculating directions in a hexagonal map with the axial representation.
 */
@RequiredArgsConstructor
public enum HexNeighbour {
    TOP_LEFT(new Point2i(0, -1)),
    TOP_RIGHT(new Point2i(+1, -1)),
    RIGHT(new Point2i(+1, 0)),
    BOTTOM_RIGHT(new Point2i(0, +1)),
    BOTTOM_LEFT(new Point2i(-1, +1)),
    LEFT(new Point2i(-1, 0)),
    ;

    private static final List<HexNeighbour> halfValues = List.of(TOP_LEFT, TOP_RIGHT, RIGHT);

    private final @NonNull Point2i directionVector;
    private HexNeighbour opposite;


    // initialize the opposite directions
    // (in a static block because Java does not support forward references for enums)
    static {
        TOP_LEFT.opposite = BOTTOM_RIGHT;
        TOP_RIGHT.opposite = BOTTOM_LEFT;
        RIGHT.opposite = LEFT;
        BOTTOM_RIGHT.opposite = TOP_LEFT;
        BOTTOM_LEFT.opposite = TOP_RIGHT;
        LEFT.opposite = RIGHT;
    }


    public static Iterable<HexNeighbour> halfValues() {
        return halfValues;
    }

    /**
     * Gets a copy of the direction vector.
     */
    public Point2i vector() {
        return new Point2i(directionVector);
    }

    /**
     * Gets the opposite direction.
     */
    public HexNeighbour opposite() {
        return opposite;
    }

    /**
     * Calculates the <em>n</em>-th neighbor in this direction and changes the original point to it. This method
     * <em>mutates</em> the original {@link Point2i}.
     *
     * @param point the original point
     * @param distance the distance from the original point; the <em>n</em> in the summary
     *
     * @return the modified original point
     *
     * @see #set(Point2i)
     * @see #of(Point2i, int)
     * @see #of(Point2i)
     */
    public @NonNull Point2i set(final @NonNull Point2i point, final int distance) {
        final Point2i actualDirection;
        if (1 == distance) {
            actualDirection = this.directionVector;
        } else {
            actualDirection = new Point2i(this.directionVector);
            actualDirection.scale(distance);
        }
        point.add(actualDirection);
        return point;
    }

    /**
     * Calculates the first neighbor in this direction and changes the original point to it. This method
     * <em>mutates</em> the original {@link Point2i}.
     *
     * @param point the original point
     *
     * @return the modified original point
     *
     * @see #set(Point2i, int)
     * @see #of(Point2i, int)
     * @see #of(Point2i)
     */
    public @NonNull Point2i set(final @NonNull Point2i point) {
        return set(point, 1);
    }

    /**
     * Calculates the <em>n</em>-th neighbor in this direction. This method creates a new {@link Point2i} and
     * <em>does not</em> mutate the original one.
     *
     * @param point the original point
     * @param distance the distance from the original point; the <em>n</em> in the summary
     *
     * @return the newly calculated point
     *
     * @see #set(Point2i, int)
     * @see #set(Point2i)
     * @see #of(Point2i)
     */
    public @NonNull Point2i of(final @NonNull Point2i point, final int distance) {
        return set(new Point2i(point), distance);
    }

    /**
     * Calculates the first neighbor in this direction. This method creates a new {@link Point2i} and <em>does not</em>
     * mutate the original one.
     *
     * @param point the original point
     *
     * @return the newly calculated point
     *
     * @see #set(Point2i, int)
     * @see #set(Point2i)
     * @see #of(Point2i, int)
     */
    public @NonNull Point2i of(final @NonNull Point2i point) {
        return of(point, 1);
    }
}
