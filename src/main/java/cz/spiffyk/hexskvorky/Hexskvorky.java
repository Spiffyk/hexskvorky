package cz.spiffyk.hexskvorky;

import lombok.*;

import javax.vecmath.Point2i;
import java.util.*;
import java.util.function.ObjIntConsumer;
import java.util.stream.Collectors;

/**
 * The Hexskvorky game logic class.
 */
@RequiredArgsConstructor
public class Hexskvorky {

    /**
     * The number of players of this game.
     */
    @Getter
    private final int numPlayers;

    /**
     * The length of the connection that wins the match.
     */
    @Getter
    private final int endScore;

    /**
     * The ID of the player whose turn it currently is.
     */
    @Getter
    private int currentPlayer = 0;

    /**
     * The player who won the match.
     */
    private int winner = -1;

    /**
     * The points that won the match.
     */
    private List<Point2i> winningPoints = null;

    /**
     * The current state of the game.
     */
    @Getter
    private @NonNull State state = State.IN_GAME;

    /**
     * A map representing the game field.
     */
    private Map<Point2i, Integer> gameField = new HashMap<>();


    /**
     * Sets the ID of the player whose turn it currently is, wrapping around according to the number of players (using
     * the modulo operation).
     *
     * @throws IllegalStateException if state is not {@link State#IN_GAME}
     */
    public void setCurrentPlayer(final int currentPlayer) {
        assertInGame();
        this.currentPlayer = currentPlayer % numPlayers;
    }

    /**
     * Gets the winner of the match.
     *
     * @throws IllegalStateException if state is not {@link State#FINISHED}
     */
    public @NonNull int getWinner() {
        assertFinished();
        return winner;
    }

    /**
     * Gets an unmodifiable list of the points that won the match.
     *
     * @throws IllegalStateException if state is not {@link State#FINISHED}
     */
    public @NonNull List<Point2i> getWinningPoints() {
        assertFinished();
        return winningPoints.stream()
                .map(Point2i::new)
                .collect(Collectors.toUnmodifiableList());
    }

    /**
     * Increments the ID of the currently playing player, wrapping around according to the number of players.
     *
     * @return the ID of the player whose turn it is after the increment
     *
     * @throws IllegalStateException if state is not {@link State#IN_GAME}
     */
    public int nextPlayer() {
        assertInGame();
        this.setCurrentPlayer(this.getCurrentPlayer() + 1);
        return this.getCurrentPlayer();
    }

    /**
     * For each hex filled with a value, performs the specified action.
     */
    public void forEachFilledHex(final @NonNull ObjIntConsumer<Point2i> action) {
        gameField.forEach((point2i, integer) -> action.accept(new Point2i(point2i), integer));
    }

    /**
     * Gets the ID of the player who filled the hex (if any) at the specified point.
     *
     * @param point the hex to get the value from
     *
     * @return the ID of the player who filled the hex, or {@link Optional#empty()} if no player did
     */
    public Optional<Integer> valueAt(final @NonNull Point2i point) {
        return Optional.ofNullable(gameField.get(point));
    }

    /**
     * Sets the value at the specified point.
     *
     * @param point the hex to set the value at
     * @param value the value; may be null to erase the value
     *
     * @throws IllegalStateException if state is not {@link State#IN_GAME}
     */
    public void setValue(final @NonNull Point2i point, Integer value) {
        assertInGame();
        gameField.put(new Point2i(point), value);
    }

    /**
     * Plays a turn of the game. If the hex on the provided point is empty, fills it with the current player's ID,
     * checks for the winning condition and starts the turn of the next player. If the hex is already filled with an ID,
     * nothing happens.
     *
     * @param point the hex to fill
     *
     * @throws IllegalStateException if state is not {@link State#IN_GAME}
     *
     * @see #getCurrentPlayer()
     * @see #valueAt(Point2i)
     * @see #setValue(Point2i, Integer)
     * @see #checkWinningCondition(Point2i)
     */
    public void play(final @NonNull Point2i point) {
        assertInGame();
        if (valueAt(point).isPresent()) {
            return;
        }

        setValue(point, currentPlayer);
        checkWinningCondition(point);
        nextPlayer();
    }

    /**
     * Checks if the specified hex (usually right after being filled) makes a winning situation and sets the winner as
     * well as the game state if it does. A winning situation occurs if the hex is a part of a line with the length
     * of at least the value of {@link #getEndScore()}.
     *
     * @param point the hex to check
     *
     * @throws IllegalStateException if state is not {@link State#IN_GAME}
     */
    public void checkWinningCondition(final @NonNull Point2i point) {
        assertInGame();
        valueAt(point).ifPresentOrElse(checkedPlayer -> {
            val winPoints = new ArrayList<Point2i>();

            for (HexNeighbour neighbour : HexNeighbour.halfValues()) {
                var score = 0;
                val directionPoints = new ArrayList<Point2i>();
                Point2i checkedPoint = new Point2i(point);
                Integer pointValue;
                do {
                    score++;
                    directionPoints.add(new Point2i(checkedPoint));
                    neighbour.set(checkedPoint);
                    pointValue = valueAt(checkedPoint).orElse(null);
                } while (Objects.equals(pointValue, checkedPlayer));

                neighbour = neighbour.opposite();
                checkedPoint.set(point);
                do {
                    score++;
                    directionPoints.add(new Point2i(checkedPoint));
                    neighbour.set(checkedPoint);
                    pointValue = valueAt(checkedPoint).orElse(null);
                } while (Objects.equals(pointValue, checkedPlayer));

                if (score >= endScore) {
                    this.winner = checkedPlayer;
                    this.state = State.FINISHED;
                    winPoints.addAll(directionPoints);
                }
            }

            if (this.state == State.FINISHED) {
                this.winningPoints = winPoints;
            }
        }, () -> {
            throw new IllegalArgumentException("The check starting point cannot be empty!");
        });
    }


    private void assertInGame() {
        assertState(State.IN_GAME);
    }

    private void assertFinished() {
        assertState(State.FINISHED);
    }

    private void assertState(@NonNull State state) {
        if (this.state != state) {
            throw new IllegalStateException(
                    String.format("This operation cannot be performed when the game state is not %s.", state.name()));
        }
    }


    /**
     * The state for the game of Hexskvorky.
     */
    public enum State {
        /**
         * The game is running and a player has a turn.
         */
        IN_GAME,

        /**
         * The game has finished and won't change anymore.
         */
        FINISHED,
    }

}
